package fr.tdr.gameOfFoods.service;

import com.google.api.core.ApiFuture;
import com.google.api.core.ApiFutures;
import com.google.cloud.firestore.*;
import com.google.firebase.cloud.FirestoreClient;
import com.google.firestore.v1.Write;
import fr.tdr.gameOfFoods.model.Recipe;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

@Service
public class RecipeService {
    public static final String COL_NAME = "recipe";



    public String addRecipe(Recipe recipe) throws InterruptedException, ExecutionException {
        Firestore dbFirestore = FirestoreClient.getFirestore();
        ApiFuture<WriteResult> collectionsApiFuture = dbFirestore.collection(COL_NAME)
                .document(recipe.getTitle()).set(recipe);
        return collectionsApiFuture.get().getUpdateTime().toString();
    }

    public Recipe getRecipe(String title) throws InterruptedException, ExecutionException {
        Firestore dbFirestore = FirestoreClient.getFirestore();
        DocumentReference documentReference = dbFirestore.collection(COL_NAME).document(title);
        ApiFuture<DocumentSnapshot> future = documentReference.get();
        DocumentSnapshot document = future.get();

        Recipe recipe;
        if(document.exists()) {
            recipe = document.toObject(Recipe.class);
            return recipe;
        } else {
            return null;
        }
    }

    public List<Recipe> getRecipes()  throws InterruptedException, ExecutionException {
        Firestore dbFirestore = FirestoreClient.getFirestore();
        List<Recipe> recipes = new ArrayList<>();
        ApiFuture<QuerySnapshot> future = dbFirestore.collection(COL_NAME).get();
        List<QueryDocumentSnapshot> documents = future.get().getDocuments();
        for(DocumentSnapshot document : documents) {
            recipes.add(document.toObject(Recipe.class));
        }

        return recipes;
    }

    public List<Recipe> getRecipesByTitle(String title)  throws InterruptedException, ExecutionException {
        Firestore dbFirestore = FirestoreClient.getFirestore();
        List<Recipe> recipes = new ArrayList<>();
        ApiFuture<QuerySnapshot> future =
                dbFirestore.collection(COL_NAME).whereEqualTo("title", title).get();
        List<QueryDocumentSnapshot> documents = future.get().getDocuments();
        for(DocumentSnapshot document : documents) {
            recipes.add(document.toObject(Recipe.class));
        }

        return recipes;
    }

    public String updateRecipe(Recipe recipe) throws InterruptedException, ExecutionException {
        Firestore dbFirestore = FirestoreClient.getFirestore();
        ApiFuture<WriteResult> collectionsApiFuture = dbFirestore.collection(COL_NAME)
                .document(recipe.getTitle()).set(recipe);
        return collectionsApiFuture.get().getUpdateTime().toString();
    }

    public String deleteRecipe(String title) {
        Firestore dbFirestore = FirestoreClient.getFirestore();
        ApiFuture<WriteResult> writeResult = dbFirestore.collection(COL_NAME)
                .document(title).delete();
        return "Document with Recipe title " +title+ " has been deleted";
    }

}
