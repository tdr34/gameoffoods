package fr.tdr.gameOfFoods.web.controller;

import fr.tdr.gameOfFoods.model.Recipe;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;

import java.util.ArrayList;
import java.util.List;

@SessionAttributes({"recipe", "recipes"})
@Controller
public class UiController {

    @ModelAttribute("recipe")
    public Recipe getRecipe() {
        return new Recipe();
    }

    @ModelAttribute("recipes")
    public List<Recipe> getRecipeList() {
        return new ArrayList<>();
    }

    @GetMapping("/")
    public String home() {
        return "home";
    }

    @GetMapping("/createRecipeUI")
    public String createRecipeUI(){
        return "formRecipe";
    }

    @GetMapping("/getRecipesUI")
    public String getRecipesUI(Model model, @ModelAttribute("recipes") List<Recipe> recipes, @ModelAttribute("recipe") Recipe recipe){
        System.out.println("------------"+recipe.getTitle()+"--------------");
        System.out.println("###########"+recipes.size()+"##############");
        model.addAttribute("recipe", recipe);
        return "listRecipe";
    }
}
