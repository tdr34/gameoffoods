package fr.tdr.gameOfFoods.web.controller;

import fr.tdr.gameOfFoods.model.Recipe;
import fr.tdr.gameOfFoods.service.RecipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

@SessionAttributes({"recipe", "recipes"})
@Controller
public class RecipeController {

    @Autowired
    RecipeService recipeService;

    @ModelAttribute("recipe")
    public Recipe getRecipe() {
        return new Recipe();
    }

    @ModelAttribute("recipes")
    public List<Recipe> getRecipeList() {
        return new ArrayList<>();
    }



    @GetMapping("/getRecipes")
    public String getRecipes(Model model, @ModelAttribute("recipes") List<Recipe> recipes, @ModelAttribute("recipe") Recipe recipe, RedirectAttributes ra) throws InterruptedException, ExecutionException {
        recipes = recipeService.getRecipes();
        recipe = recipes.get(0);

        model.addAttribute("recipe", recipe);
        model.addAttribute("recipes", recipes);
        return "listRecipe";
    }

    @GetMapping("/getRecipesByTitle")
    public List<Recipe> getRecipesByTitle(@RequestParam String title) throws InterruptedException, ExecutionException {
        return recipeService.getRecipesByTitle(title);
    }

    @GetMapping("/getRecipeDetails")
    public String getRecipeDetails(Model model, @ModelAttribute("recipe") Recipe recipe) {
        model.addAttribute("recipe", recipe);
        return "recipeDetails";
    }

    @RequestMapping("/createRecipe")
    public String createRecipe(@ModelAttribute("recipe") Recipe recipe, HttpServletRequest request) throws InterruptedException, ExecutionException {
        recipe.setTitle(request.getParameter("title"));
        recipe.setTime(request.getParameter("time"));
        recipe.setRecipe(request.getParameter("recipeText"));
        recipe.setCost(Integer.parseInt(request.getParameter("cost")));
        recipe.setDifficulty(Integer.parseInt(request.getParameter("difficulty")));
        recipe.setNbPeople(Integer.parseInt(request.getParameter("nbPeople")));
        recipeService.addRecipe(recipe);

        return "redirect:/getRecipes";
    }

    @PutMapping("/updateRecipe")
    public String updateRecipe(@RequestParam Recipe recipe)  throws InterruptedException, ExecutionException {
        return recipeService.updateRecipe(recipe);
    }

    @DeleteMapping("/deleteRecipe")
    public String deleteRecipe(@RequestParam String title)  throws InterruptedException, ExecutionException {
        return recipeService.deleteRecipe(title);
    }
}

